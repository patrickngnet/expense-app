import { useState } from "react";
import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
  const saveExpenseHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };

    props.onAddExpense(expenseData);
  };

  const [isOpenExpense, setOpenExpense] = useState(false);

  const toggleExpense = () => {
    setOpenExpense((prevOpenExpense) => !prevOpenExpense);
  };
  return (
    <div className="new-expense">
      {!isOpenExpense && (
        <div className="new-expense__actions-center">
          <button onClick={toggleExpense}>Add new expense</button>
        </div>
      )}

      {isOpenExpense && (
        <ExpenseForm
          onToggleExpense={toggleExpense}
          onSaveExpenseData={saveExpenseHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;
